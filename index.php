<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CD Katalógus</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <style>
            td {
                text-align: center;
            }
            thead {
                text-align: center;
            }
        </style>
    </head>
    <body>
<?php
include('login.php');

// kapcsolódás a MySQL szerverhez
$conn = mysqli_connect($host, $username, $password);
if (!$conn)
{
    die("Connection failed: ".mysqli_connect_error());
}
mysqli_select_db($conn, $dbname);
//---------------------------------

// tábla létrehozása
$sql = "CREATE TABLE IF NOT EXISTS gyujtemeny (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    eloado VARCHAR(50),
    album VARCHAR(50),
    evjarat SMALLINT,
    cim VARCHAR(50),
    stilus VARCHAR(25),
    zenedb SMALLINT
)";
mysqli_query($conn, $sql);
//--------------------


if (!mysqli_query($conn, $sql))
{
    echo "<h5 style='color: red'>Nem sikerült létrehozni a táblát!</h5>";
}

//oldal léptetés
if (!isset ($_GET['page']) ) {  
    $page = 1;  
} else {  
    $page = $_GET['page'];  
}
$re = 5;
$page_first_result = ($page-1) * $re;
$query = "SELECT * FROM gyujtemeny";
$result = mysqli_query($conn, $query);
$number_of_result = mysqli_num_rows($result);
$number_of_page = ceil($number_of_result / $re);
$query = "SELECT * FROM gyujtemeny LIMIT " . $page_first_result . ',' . $re;
$result = mysqli_query($conn, $query);
//---------------

//törlés
if (isset($_POST['torles']))
{
    $sql = "DELETE FROM gyujtemeny WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $_POST['torles']);
    $stmt->execute();
    //header("Refresh:0");
    echo "<script type='text/javascript'>window.location.href = window.location.href;</script>";
}
//------------------

//módosítás

if (isset($_POST['modositas']))
{
    /*
    session_start();
    $mod_id = $_POST['modositas'];
    $_SESSION['id'] = $mod_id;
    header('Location: modify.php');
    */
    echo "<script type='text/javascript'>window.location.href = 'modify.php?SorId=".$_POST['modositas']."';</script>";
}
//---------

//keresési sáv
echo '
<nav class="navbar bg-light">
  <div class="container-fluid">
    <a href="append.php" class="navbar-brand">Hozzáadás</a>
    <form class="d-flex" role="search" method="POST">
      <input type="search" name="2search" class="form-control me-2" placeholder="Keresés..." aria-label="Search">
      <button class="btn btn-outline-success" type="submit" name="search_btn" value="'.$_POST['2search'].'">Keres</button>
    </form>
  </div>
</nav>
<br>
<br>
<br>
';
//------------

//ha Keresés gomb -> átirányítás a search.php oldalra

if (isset($_POST['search_btn']))
{
    //header('Location: search.php?keresett='.$_POST['2search']);
    echo "<script type='text/javascript'>window.location.href = 'search.php?keresett=".$_POST['2search']."';</script>";
}


//táblázat kiirása
if (mysqli_num_rows($result) > 0)
{
    echo "<center>";
    echo "<form method='POST'>";
    echo "<table class='table'>";
    echo "
    <thead class='table-dark'>
        <th>ID</th>
        <th>Előadó</th>
        <th>Album</th>
        <th>Évjárat</th>
        <th>Cím</th>
        <th>Stílus</th>
        <th>Zene darab</th>
        <th>#</th>
        <th>#</th>
    </thead>
    ";

    while ($sor = mysqli_fetch_assoc($result))
    {
        echo "<tr>";
        foreach ($sor as $adat)
        {
            echo "<td>".htmlspecialchars($adat)."</td>";
        }
        //törlés gomb
        echo "<td>
        <button class='btn btn-sm btn-outline-secondary' type='submit' name='torles' value='".$sor['id']."'>Törlés</button>
        </td>
        <!-- módosítás gomb -->
        <td>
        <button class='btn btn-sm btn-outline-secondary' type='submit' name='modositas' value='".$sor['id']."'>Módosítás</button>
        </td>
        ";
        
        echo "</tr>";
        //-------------
        
    }
    echo "</table>";
    echo "</form>";
    //-----------------------
    //oldal léptető gombok
    for($page = 1; $page<= $number_of_page; $page++)
    {  
        echo '<a href="index.php?page='.$page.'"><button class="btn btn-sm btn-outline-secondary">'.$page.'</button></a> ';
    }
    //---------------------
}
else
{
    echo "Nincs rögzitett gyűjtemény.";
}
?>

</form>
</center>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>