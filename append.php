<?php
include('login.php');

// kapcsolódás a MySQL szerverhez
$conn = mysqli_connect($host, $username, $password);
if (!$conn)
{
    die("Connection failed: ".mysqli_connect_error());
}
mysqli_select_db($conn, $dbname);
//---------------------------------

echo "
<center>
<form action='' method='POST'>
<table>

<tr>
<th>Előadó</th>
<th>Album</th>
<th>Évjárat</th>
<th>Cim</th>
<th>Stilus</th>
<th>Zene darab</th>
</tr>

<tr>
<td>
<input list='eloadok' name='eloado'>
<datalist id='eloadok'>
";
$sql = "SELECT eloado FROM gyujtemeny";
$eloado = mysqli_query($conn, $sql) or die(mysqli_error());
while ($sor = mysqli_fetch_array($eloado))
{
    echo "<option value='".$sor[0]."'>";
}
echo "
</datalist>
</td>

<td>
<input type='text' name='album'>
</td>

<td>
<input type='text' name='evjarat'>
</td>

<td>
<input type='text' name='cim'>
</td>

<td>
<input list='stilusok' name='stilus'>
<datalist id='stilusok'>
";
$sql = "SELECT stilus FROM gyujtemeny";
$stilus = mysqli_query($conn, $sql) or die(mysql_error());
while ($sor = mysqli_fetch_array($stilus))
{
    echo "<option value='".$sor[0]."'>";
}
echo "
</td>

<td>
<input type='text' name='db'>
</td>

</tr>
</table>

<p><input type='submit' value='Hozzáadás' name='hozzaad'></p>
</form>
</center>
";

$sql = "INSERT INTO gyujtemeny (eloado, album, evjarat, cim, stilus, zenedb) VALUES ('".htmlspecialchars($_POST['eloado'])."', '".htmlspecialchars($_POST['album'])."', '".htmlspecialchars($_POST['evjarat'])."', '".htmlspecialchars($_POST['cim'])."', '".htmlspecialchars($_POST['stilus'])."', '".htmlspecialchars($_POST['db'])."')";
//mysqli_query($conn, $sql);

if (isset($_POST['hozzaad']))
{
    if (mysqli_query($conn, $sql))
    {
        //header('Location: index.php');
        echo "<script type='text/javascript'>window.location.href = 'index.php';</script>";
    }
    echo "<script>alert('Nem sikerült hozzáadni a gyűjteményt!')</script>";
    exit();
}
?>