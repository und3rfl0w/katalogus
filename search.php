<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Keresés</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <style>
            td {
                text-align: center;
            }
            thead {
                text-align: center;
            }
        </style>
    </head>
    <body>

<?php
include('login.php');

// kapcsolódás a MySQL szerverhez
$conn = mysqli_connect($host, $username, $password);
if (!$conn)
{
    die("Connection failed: ".mysqli_connect_error());
}
mysqli_select_db($conn, $dbname);
//---------------------------------

//session_start();
$text = $_GET['keresett'];

$sql = " SELECT * FROM gyujtemeny WHERE eloado LIKE '%$text%' OR album LIKE '%$text%' OR evjarat='$text' OR cim LIKE '%$text%' OR stilus LIKE '%$text%' OR zenedb='$text'";
$table = mysqli_query($conn, $sql);

//törlés
if (isset($_POST['torles']))
{
    $sql = "DELETE FROM gyujtemeny WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $_POST['torles']);
    $stmt->execute();
    //header("Refresh:0");
    echo "<script type='text/javascript'>window.location.href = window.location.href;</script>";
}
//------------------

//módosítás

if (isset($_POST['modositas']))
{
    /*
    session_start();
    $mod_id = $_POST['modositas'];
    $_SESSION['id'] = $mod_id;
    header('Location: modify.php');
    */
    echo "<script type='text/javascript'>window.location.href = 'modify.php?SorId=".$_POST['modositas']."';</script>";
}
//---------


//táblázat kiirása
if (mysqli_num_rows($table) > 0)
{
    echo "<center>";
    echo "<form method='POST'>";
    echo "<table class='table'>";
    echo "
    <thead class='table-dark'>
        <th>ID</th>
        <th>Előadó</th>
        <th>Album</th>
        <th>Évjárat</th>
        <th>Cim</th>
        <th>Stilus</th>
        <th>Zene darab</th>
        <th>#</th>
        <th>#</th>
    </thead>
    ";
    while ($sor = mysqli_fetch_assoc($table))
    {
        echo "<tr>";
        foreach ($sor as $adat)
        {
            echo "<td>$adat</td>";
        }
        //törlés gomb
        echo "<td>
        <button class='btn btn-sm btn-outline-secondary' type='submit' name='torles' value='".$sor['id']."'>Törlés</button>
        </td>
        <!-- módosítás gomb -->
        <td>
        <button class='btn btn-sm btn-outline-secondary' type='submit' name='modositas' value='".$sor['id']."'>Módosítás</button>
        </td>
        ";
        
        echo "</tr>";
        //-------------
        
    }
    echo "</table>";
    echo "</form>";
    echo "</center>";
    //-----------------------
}
else
{
    echo "Nincs rögzitett gyűjtemény, vagy nincs a keresésnek megfelelő találat.<br>";
}

echo "<br><a href='index.php'><button style='margin-left: 40px' class='btn btn-sm btn-outline-secondary'>Vissza</button></a>";

?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>